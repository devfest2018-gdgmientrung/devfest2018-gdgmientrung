let contractInstance;
let arrAddr = [];
let arrName = [];
let _arrVoterAddress = [];
let byteCode;
let currentAccount;
const CTABI = [{ "constant": false, "inputs": [{ "name": "proposal", "type": "uint256" }], "name": "vote", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [{ "name": "", "type": "uint256" }], "name": "proposals", "outputs": [{ "name": "pAddress", "type": "address" }, { "name": "name", "type": "bytes32" }, { "name": "voteCount", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "", "type": "uint256" }], "name": "winnerAddresses", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "startDate", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "getWinnerAddresses", "outputs": [{ "name": "", "type": "address[]" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "isFinish", "outputs": [{ "name": "", "type": "bool" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "chairperson", "outputs": [{ "name": "", "type": "address" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "person", "type": "address" }], "name": "getWeightOf", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "winningProposal", "outputs": [{ "name": "winningProposal_", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "contractName", "outputs": [{ "name": "", "type": "string" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "", "type": "address" }], "name": "voters", "outputs": [{ "name": "weight", "type": "uint256" }, { "name": "indexVote", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [], "name": "setWinnerAddresses", "outputs": [], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "endDate", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "numberOfCandidate", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "inputs": [{ "name": "_name", "type": "string" }, { "name": "proposalAddresses", "type": "address[]" }, { "name": "proposalNames", "type": "bytes32[]" }, { "name": "voterAddresses", "type": "address[]" }, { "name": "_startDate", "type": "uint256" }, { "name": "_endDate", "type": "uint256" }], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }];

let dataInstance = async (_data) => {
    return new Promise((resolve, reject) => {
        BrowserSolc.getVersions(function (soljsonSources, soljsonReleases) {
            BrowserSolc.loadVersion("soljson-v0.4.25+commit.59dbf8f1.js", function (compiler) {
                source = _data;
                optimize = 1;
                result = compiler.compile(source, optimize);
                resolve(result);
            });
        });
    });
}

//get data in Ballot.sol
function getData(path) {
    return new Promise((resolve, reject) => {
        $.get(path, function (data) {
            // console.log(data);

            resolve(data);
        }, 'text');
    })
}
//get name of contract
const getContractName = (_con) => {
    return new Promise((resolve, reject) => {
        _con.contractName.call((err, data) => {
            if (err) resolve(err);
            resolve(data);
        });
    });
}
//get name proposal[]
const getName = (_con, index) => {
    return new Promise((resolve, reject) => {
        _con.proposals.call(index, (err, data) => {
            if (err) resolve(err);
            resolve(web3.toAscii(data[1]));
        });
    });
}

//get vote of a proposal
const getVote = (_con, index) => {
    return new Promise((resolve, reject) => {
        _con.proposals.call(index, (err, data) => {
            if (err) resolve(err);
            resolve(data[2]);
        });
    });
}

const getWeight = (_con, person) => {
    return new Promise((resolve, reject) => {
        _con.getWeightOf.call(person, (err, data) => {
            if (err) resolve(err);
            resolve(data);
        });
    });
}

//get chairperson address
const getChairPerson = (_con) => {
    return new Promise((resolve, reject) => {
        _con.chairperson.call((err, data) => {
            if (err) resolve(err);
            resolve(data);
        });
    });
}
//check a contract is finish yet ?
const getFinish = (_con) => {
    return new Promise((resolve, reject) => {
        _con.isFinish.call((err, data) => {
            if (err) resolve(err);
            resolve(data);
        });
    });
}


const voteForCandidate = (_con, index) => {
    return new Promise((resolve, reject) => {
        _con.vote.sendTransaction(index, (err, data) => {
            if (err) resolve(err);
            resolve(data);
        });
    });
}

//set winner of a contract
const setWinner = (_con) => {
    return new Promise((resolve, reject) => {
        _con.setWinnerAddresses.sendTransaction((err, data) => {
            if (err) resolve(err);
            // console.log(web3.toAscii(data));
            resolve(data);
        });
    })
}

//get winner names
const getWinName = (_con) => {
    // console.log("here");
    return new Promise((res, rej) => {
        _con.getWinnerAddresses.call((er, data) => {
            if (er) res(er);
            res(data);
        })
    })
}

//get number of Candidate in contract
const getNum = (_con) => {
    return new Promise((resolve, reject) => {
        _con.numberOfCandidate.call((err, data) => {
            if (err) resolve(err);
            resolve(data);
        });
    })
}

//get close Date
const getCloseDate = (_con) => {
    return new Promise((resolve, reject) => {
        _con.endDate.call((err, data) => {
            if (err) resolve(err);
            resolve(data);
        });
    })
}
//get start Date
const getStartDate = (_con) => {
    return new Promise((resolve, reject) => {
        _con.startDate.call((err, data) => {
            if (err) resolve(err);
            resolve(data);
        });
    })
}

const getAccounts = () => {
    return new Promise((resolve, reject) => {
        web3.eth.getAccounts((err, res) => {
            resolve(res[0]);
        });
    })

}


window.onload = async function () {

    if (typeof web3 !== 'undefined') {

        web3 = new Web3(web3.currentProvider);
        // provider.enable();
    } else {
        web3 = new Web3(new Web3.providers.HttpProvider("https://rinkeby.infura.io/"));
    }

    if (typeof BrowserSolc == 'undefined') {
        console.log("You have to load browser-solc.js in the page.  We recommend using a <script> tag.");
        throw new Error();
    }
    currentAccount = await getAccounts();
    $('#accountAddress').text(`Your account : ${currentAccount}`);


}

async function clickTest(inforvtAddr) {
    currentAccount = await getAccounts();
    contractInstance = web3.eth.contract(CTABI).at(inforvtAddr);
    let ctName = await getContractName(contractInstance);
    let ctCreator = await getChairPerson(contractInstance);
    $('#inforvtAddr').append(`<td>${ctName}</td>`);
    $('#inforcreatorAddr').append(`<td>${ctCreator}</td>`);
    $('#inforvtAddr').append(`<td>${inforvtAddr}</td>`);
    let num = await getNum(contractInstance);
    for (let i = 0; i < num; i++) {
        let vtName = await getName(contractInstance, i);
        let vtVote = await getVote(contractInstance, i);
        $('#cadidateList tbody').append(`<tr><td>${vtName}</td><td>${vtVote}</td><td><input type="checkbox" value=${i} class="chk"></td></tr>`);
    }
    let number = await getWeight(contractInstance, currentAccount);
    console.log(typeof number);

    $("#alertcheck").text(`You have ${number} right to vote`);

}


//click to a contract
async function loadContract(_addr) {

    contractInstance = web3.eth.contract(ABI).at(_addr);
    let votingName = await getContractName(contractInstance);
    let votingAddress = _addr;
    let _isFinish = await getChairPerson(contractInstance);
    //get number of proposal in contract
    let num = await getNum(contractInstance);

    // load infomation of contract to table
    for (let i = 0; i < num; i++) {
        let _cNameHex = await getName(contractInstance, i);
        let _cName = web3.toAscii(_cNameHex);
        let _cVote = await getVote(contractInstance, i);
        let candidateTemplate = `<tr data-id-vode=${i}><th>${i + 1}</th><td>${_cName}</td><td>${_cVote}</td><td><input type="checkbox" class="checkvote" name="check"></td></tr>`;
        $('#cadidateList tbody').append(candidateTemplate);
    }
}

async function clickVote() {
    $(".chk:checked").each(function () {
        console.log($(this).val());

        index = $(this).val();
    });
    await voteForCandidate(contractInstance, index);
    $('#cadidateList tbody').empty();
    $('#voteDetails tbody').empty();
}

async function clickLaunch() {
    // console.log(currentAccount);
    let fileContract = await getData('../../static/contracts/Ballot.sol');
    let contractCompile = await dataInstance(fileContract);
    byteCode = contractCompile.contracts[':Ballot'].bytecode;
    let votingName = $('#eName').val();
    let txtStart = $('#startDate').val();
    let txtEnd = $('#endDate').val();
    let strStart = new Date(txtStart);
    let _startDate = Date.parse(strStart);
    let strEnd = new Date(txtEnd);
    let _endDate = Date.parse(strEnd);

    var tableVoter = $("#data-table-voter tbody");
    tableVoter.find('tr').each(function (i, el) {
        var $tds = $(this).find('td'),
            tbVoterAddr = $tds.eq(1).text();
        _arrVoterAddress.push(tbVoterAddr);
    });

    var tableProposal = $("#data-table tbody");
    tableProposal.find('tr').each(function (i, el) {
        var $tds = $(this).find('td'),
            tbName = $tds.eq(0).text(),
            tbAddr = $tds.eq(1).text();
        arrName.push(tbName);
        arrAddr.push(tbAddr);
    });

    let contract = web3.eth.contract(CTABI);
    contract.new(
        votingName,
        arrAddr,
        arrName,
        _arrVoterAddress,
        _startDate,
        _endDate,
        {
            data: `0x${byteCode}`,
            from: currentAccount,
            gas: 4900000
        }, async (err, res) => {

            if (res.address) {
                $('#sendmessage').text(`Your voting has been created. Your Voting Address is: ${res.address}. Check out it right below!`)
                $("#sendmessage").show();
                postData = {
                    startDate: txtStart,
                    endDate: txtEnd,
                    votingAddress: res.address,
                    votingName: votingName,
                    creator: `${currentAccount}`,
                    isFinish: false
                };
                $.ajax({
                    type: 'POST',
                    url: '/createVoting',
                    data: JSON.stringify(postData),
                    dataType: "json",
                    success: function (resultData) { resolve(resultData); }
                });
            }
        });

    arrName = [];
    arrAddr = [];
    _arrVoterAddress = [];
}
