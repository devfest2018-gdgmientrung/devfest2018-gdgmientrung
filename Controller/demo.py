from cmath import phase
import numpy as np
from itertools import product
from collections import Counter
import math

from functools import reduce


def func():
    A = np.array(input().split(), dtype=int)
    B = np.array(input().split(), dtype=int)
    print(*list(product(A,B)))

def func1():
    K, M = map(int, input().split())
    N = (list(map(int, input().split()))[1:] for _ in range(K))
    result = (map(lambda x: sum(i**2 for i in x)%M, product(*N)))
    print(max(result))

def func2():
    S, N = input(), int(input())
    for part in zip(*[iter(S)] * N):
        d = dict()
        print(''.join([d.setdefault(c, c) for c in part if c not in d]))
def func3():
    from collections import defaultdict
    d = defaultdict(list)
    d['python'].append("awesome")
    d['something-else'].append("not relevant")
    d['python'].append("language")
    for i in d.items():
        print(i)
#
def test (x, y):
    print(x, y)
    return x * y

def func4():
    x = input()
    print("{}\n{}".format(abs(complex(x)), phase(complex(x))))

def func5():
    AB = int(input())
    BC = int(input())
    print("{}{}".format(round(math.degrees(math.atan2(AB,BC))),'°'))

def func6(n):
    [print("{}".format(int('1' * i)**2)) for i in range(1, int(input())+1)]

def func7():
    a, b, c = [int(input()) for _ in  range(3)]
    print("{}\n{}".format(a**b,pow(a,b,c)))

if __name__ == '__main__':
    # func3()
    # from functools import reduce
    # p = reduce(lambda x,y: test(x,y), [1,2,3])
    # print(p)
    func7()